# Ansible Task
- Time 60-90 min

- Infrastructure as Code with Ansible on prem, hybrid and public cloud Video 
- We have successfully implemented on **Prem** and **Hybrid cloud infrastructure** already
- Time to move on migrating everything to AWS now

## Task

- Migrate Hybrid cloud infrastructure to AWS
- Create an Ansible controller on ec2 instance 
- Creating 2 machines on EC2 with playbooks 
- Provisioning one with we webapp
- provisioning one with mongodb
- Provision Configuration of environment variables (DB_HOST & config files and others) 

## Deliverables 

- Record Video:
 - what is infra structure as code and Ansible
 - what is on prem, hybrid and public cloud infrastructure and pros and cons (create a diagram for on prem, hybrid and public cloud) 
 - Ansible Basics and workings
 - Ansible multiple hosts running playbook
 - Woking app with db in AWS cloud

## Task Solution

### Creating the Ansible controller on AWS
- Can be created manually or automatically with Packer
- Allow SSH access from `My IP`

### Installing Ansible on the controller
```bash
sudo apt-add-repository ppa:ansible/ansible
sudo apt update –y
sudo apt install ansible
```

### Setting up the Ansible Vault
- `cd /etc/ansible`
- `mkdir group-vars`
- `cd group-vars`
- `mkdir all`
- `cd all`
- `sudo ansible-vault create pass.yml`
- Enter aws access and secret key

### Creating the `create_app_ec2.yml` file
```yml
# playbook for launching the 'app' aws ec2 instance
---
- hosts: localhost
  connection: local
  gather_facts: True
  become: True
  vars:
    key_name: eng89_devops
    region: eu-west-1
# specifying the ami, security group and subnet
# can also specify pre-built ami to automate configuration
    image: ami-0101ad4503dd3db59
    id: " eng89 ansible playbook for launching app EC2"
    sec_group: "sg-00b13b15d4b418c2f"
    subnet_id: "subnet-0865a37417a0f1d80"
    ansible_python_interpreter: /usr/bin/python3
  tasks:
     - name: facts
      block:
       - name: get instance gather_facts
        ec2_instance_facts:
          aws_access_key: "{{aws_access_key}}"
          aws_secret_key: "{{aws_secret_key}}"
          region: "{{ region }}"
        register: result
     - name: provisioning ec2 instances
      block:
       - name: upload public key to aws_access_key
        ec2_key:
          name: "{{ key_name }}"
          key_material: "{{ lookup('file', '~/.ssh/{{ key_name }}.pub') }}"
          region: "{{ region }}"
          aws_access_key: "{{aws_access_key}}"
        - name: provision instance
        ec2:
          aws_access_key: "{{aws_access_key}}"
          aws_secret_key: "{{aws_secret_key}}"
          assign_public_ip: True
          key_name: "{{ key_name }}"
          id: "{{ id }}"
          vpc_subnet_id: "{{ subnet_id }}"
          group_id: "{{ sec_group }}"
          image: "{{ image }}"
          instance_type: t2.micro
          region: "{{ region }}"
          wait: True
          count: 1
          instance_tags:
            Name: eng89_raymond_ansible_playbook_app
       tags: ['never', 'create_ec2']
```